<?php

// Pre-populate form data with persisted data.
$form_data = [
    'auth_server_url' => null,
    'property' => null,
    'enabled' => false,
    'sites' => [],
];
$settings = $data['settings'];
if (isset($settings)) {
    $form_data['auth_server_url'] = $settings->authServer()->url();
}
if (isset($settings) && $settings instanceof \Kore\Plugin\Settings\SiteSettings) {
    $form_data['property'] = $settings->property();
    $form_data['enabled'] = $settings->enabled();
}
if (isset($settings) && $settings instanceof \Kore\Plugin\Settings\MultisiteSettings) {
    $sites = [];
    foreach ($settings->sites() as $site) {
        $sites[$site->blog()->toInteger()] = [
            'blog' => $site->blog()->toInteger(),
            'property' => $site->hasProperty() ? $site->property()->toString() : null,
            'enabled' => $site->enabled(),
        ];
    }
    $form_data['sites'] = $sites;
}

// Override form data with old request data.
if ( isset($data['results']) ) {
    $form_data['auth_server_url'] = $data['results']['data']['auth_server_url'];
    if (isset($data['results']['data']['property'])) {
        $form_data['property'] = $data['results']['data']['property'];
    }
    $form_data['enabled'] = !empty($data['results']['data']['property']);
    if (isset($data['results']['data']['sites'])) {
        $form_data['sites'] = $data['results']['data']['sites'];
    }
}

?>
<div class="wrap">
    <h1>Kore Single Sign-On: Settings</h1>
    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
        <ul>
            <li>
                <label for="auth_server_url">Auth Server URL:</label>
                <input type="url" name="auth_server_url" id="auth_server_url" value="<?php echo $form_data['auth_server_url']; ?>">
                <?php if ( ! empty($data['results']['errors']['auth_server_url'])): ?><span class="msg-error"><?php echo $data['results']['errors']['auth_server_url']; ?></span><?php endif; ?>
            </li>
            <?php if (function_exists('is_multisite') && is_multisite()): ?>
                <h3>Select sites to use Kore SSO authentication:</h3>
                <table class="widefat fixed" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="manage-column column-columnname" scope="col">Site</th>
                        <th class="manage-column column-columnname" scope="col">Property ID</th>
                        <th class="manage-column column-columnname" scope="col">Enabled</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($data['sites']); $i++): ?>
                        <?php
                        $site = $data['sites'][$i];
                        if ($site instanceof WP_Site) {
                            $site = (array) $site;
                        }
                        ?>
                        <tr <?php if ($i % 2 === 0): ?>class="alternate"<?php endif; ?>>
                            <th class="column-columnname">
                                <code><?php
                                    $url = $site['domain'] . $site['path'];
                                    echo sprintf('<a href="http://%s">%s</a>', $url, $url);
                                    ?></code>
                                <input
                                    type="hidden"
                                    name="sites[<?php echo $site['blog_id']; ?>][blog]"
                                    value="<?php echo $site['blog_id']; ?>"
                                >
                            </th>
                            <td class="column-columnname">
                                <input
                                    type="text"
                                    name="sites[<?php echo $site['blog_id']; ?>][property]"
                                    value="<?php echo isset($form_data['sites'][$site['blog_id']]) ? $form_data['sites'][$site['blog_id']]['property'] : null; ?>"
                                >
                                <?php if ( ! empty($data['results']['errors']['sites'][$site['blog_id']]['property'])): ?><span class="msg-error"><?php echo $data['results']['errors']['sites'][$site['blog_id']]['property']; ?></span><?php endif; ?>
                            </td>
                            <td class="column-columnname">
                                <input
                                    type="checkbox"
                                    name="sites[<?php echo $site['blog_id']; ?>][enabled]"
                                    <?php if ( ! empty($form_data['sites'][$site['blog_id']]['enabled'])) : ?>
                                        checked="checked"
                                    <?php endif; ?>
                                >
                            </td>
                        </tr>
                    <?php endfor; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <li>
                    <label for="property">Property ID:</label>
                    <input type="text" name="property" id="property" value="<?php echo $form_data['property']; ?>">
                    <?php if ( ! empty($data['results']['errors']['property'])): ?><span class="msg-error"><?php echo $data['results']['errors']['property']; ?></span><?php endif; ?>
                </li>
                <li>
                    <label for="enabled">Enabled (once enabled, you will not be able to login via the ordinary Wordpress login):</label>
                    <input type="checkbox" id="enabled" name="enabled" <?php if ($form_data['enabled']): ?>checked="checked"<?php endif; ?>>
                </li>
            <?php endif; ?>
        </ul>
        <p><input type="submit" name="kore_settings" value="Save"></p>
    </form>
</div>
