<?php

use Kore\Infrastructure\ServerExistenceChecker;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 */
class Kore_Admin {

    /**
     * The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     */
    private $version;

    /**
     * The slug for the plugin
     */
    private $slug = 'kore-sso';

    private $kore_database;

    /**
     * Initialize the class and set its properties.
     *
     * @param  string  $plugin_name  The name of this plugin.
     * @param  string  $version      The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->kore_database = new Kore_Database();
    }

    /**
     * Register the stylesheets for the admin area
     */
    public function enqueue_styles() {
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kore-admin.css', array(), $this->version, 'all' );
    }

    /**
     * Create an admin menu for the Kore SSO plugin.
     */
    public function kore_menu() {
        add_menu_page('Kore Single Sign-On', 'Kore SSO', 'manage_options', $this->slug, array( $this, 'view_index') );
        add_submenu_page( $this->slug, 'Settings', 'Settings', 'manage_options', 'kore-settings', array( $this, 'view_settings') );
    }

    /**
     * Access check used by all views.
     *
     */
    private function access_check() {
        if ( !current_user_can( 'manage_options' ) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }
    }

    /**
     * Index page for Kore SSO.
     *
     */
    public function view_index() {
        $this->access_check();

        $settings = \Kore\Plugin\Helpers::settings();

        if ( ! is_null($settings)) {

            if ( ! ServerExistenceChecker::baseUrlExistsAndIsRunningKore($settings->authServer()->url()->toString())) {
                $this->notice_base_url_error();
            }  else {
                $this->notice_base_url_success();
            }
        }

        $this->output_view('index');
    }

    /**
     * Settings page for Kore SSO.
     *
     */
    public function view_settings() {

        $this->access_check();
        $results = $this->handle_settings_form_submission();

        $settings = \Kore\Plugin\Helpers::settings();

        // IF dealing with a multisite install THEN we need to gather all the sub-sites for configuration.
        $sites = [];
        if (\Kore\Plugin\Helpers::isMultisite()) {
            if ( ! function_exists('get_sites') && ! function_exists('wp_get_sites')) {
                $this->notice_output('error', 'Your Wordpress install is setup as multi-site. You may need to <a href="/wp-admin/network.php">configure your network</a>. Specifically, this installation does not have the required <code>get_sites</code> or <code>wp_get_sites</code> functions available.');
                return;
            }
            $sites = function_exists('get_sites') ? get_sites() : wp_get_sites();
        }

        $this->output_view('settings', [
            'results' => $results,
            'settings' => $settings,
            'sites' => $sites
        ]);
    }

    /**
     * Handler for outputting view files.
     *
     * @param string $view_name
     * @param array $data
     */
    private function output_view($view_name, array $data = [])
    {
        require_once( plugin_dir_path( dirname( __FILE__ ) ).'admin/views/'.$view_name.'.php' );
    }

    /**
     * Handles form submission from settings page.
     *
     * @return mixed(array|null)
     */
    private function handle_settings_form_submission() {

        if ( isset($_POST['kore_settings']) ) {

            $handler = \Kore\Plugin\Helpers::isMultisite()
                ? new \Kore\Plugin\Settings\MultisiteSettingsFormHandler
                : new \Kore\Plugin\Settings\SiteSettingsFormHandler;
            $result = $handler->handle($_POST);

            // IF there are no errors THEN persist the settings.
            if (empty($result->errors)) {
                if (\Kore\Plugin\Helpers::isMultisite()) {
                    $settings = \Kore\Plugin\Settings\MultisiteSettingsFactory::fromFormRequestData($result->data);
                    $store = new \Kore\Plugin\Settings\MultisiteSettingsStore;
                    $store->save($settings);
                } else {
                    $settings = \Kore\Plugin\Settings\SiteSettingsFactory::fromFormRequestData($result->data);
                    $store = new \Kore\Plugin\Settings\SiteSettingsStore;
                    $store->save($settings);
                }
                $this->notice_settings_updated();
            }

            // IF there were errors THEN show a notice.
            if ( ! empty($errors)) {
                $this->notice_settings_save_error();
            }

            return array(
                'data' => $result->data,
                'errors' => $result->errors,
            );
        }

        return null;
    }

    /* -------------------------------------------------------------------------
     * ADMIN NOTICES
     * ---------------------------------------------------------------------- */

    /**
     * Output a notice.
     *
     * @param string $class
     * @param string $message
     */
    private function notice_output($class, $message)
    {
        echo "<div class=\"$class\"> <p>$message</p></div>";
    }

    /**
     * Admin notice for when plugin has not yet been configured.
     */
    public function notice_plugin_settings_required()
    {
        $settings = \Kore\Plugin\Helpers::settings();
        if (is_null($settings)) {

            $path = '/wp-admin/admin.php?page=kore-sso';
            if (\Kore\Plugin\Helpers::isMultisite()) {
                $path = '/wp-admin/network/admin.php?page=kore-sso';
            }

            $message = '<a href="'.$path.'">Kore SSO</a> is not yet configured. Users will be unable to authenticate.';
            $this->notice_output('error', $message);
        }
    }

    /**
     * Admin notice for when plugin settings are successfully update.
     */
    public function notice_settings_updated() {
        $class = "updated";
        $message = "Settings updated successfully.";
        $this->notice_output($class, $message);
    }

    /**
     * Admin notice for when plugin settings are incorrectly entered.
     */
    public function notice_settings_save_error() {
        $class = "error";
        $message = "Settings could not be updated, please check your form data.";
        $this->notice_output($class, $message);
    }

    /**
     * Admin notice for when base URL is incorrectly set.
     */
    public function notice_base_url_error($errors = []) {
        $class = "error";
        $message = "Kore cannot be found. The service is either not running or the <strong>Kore base URL</strong> is incorrect.";

        if ( ! empty($errors)) {
            $message .= '<ul style="margin: 0; padding: 0 0 0 1.75em; list-style-type: disc;">';
            foreach ($errors as $error) {
                $message .= sprintf('<li>%s</li>', $error);
            }
            $message .= '</ul>';
        }

        $this->notice_output($class, $message);
    }

    /**
     * Admin notice for when base URL is incorrectly set.
     */
    public function notice_base_url_success() {
        $class = "updated";
        $message = "Kore service is running.";
        $this->notice_output($class, $message);
    }

    public function notice_admin_warnings()
    {
        $isKoreSettingsPage = preg_match(
            '/(admin).php/',
            $_SERVER['SCRIPT_NAME']) && isset($_GET['page']) && in_array($_GET['page'],
            array('kore-sso', 'kore-settings')
        );

        // warn about login page edit
        if (
            \Kore\Plugin\Settings\AuthServerVerification::isDisabled()
            && $isKoreSettingsPage
        ) {
            $message = "<strong>Warning:</strong> Kore auth server verification has been disabled.";
            $this->notice_output('notice  notice-warning', $message);
        }
    }


    /* -------------------------------------------------------------------------
     * WP REST API - EXTENSIONS
     * ---------------------------------------------------------------------- */

    /**
     * Custom routes for WP REST API.
     */
    public function kore_api_routes() {

        register_rest_route( 'wp/v2', 'network/users', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array($this, 'get_network_users'),
            'permission_callback' => array($this, 'get_network_users_permissions_check'),
        ));

        register_rest_route( 'wp/v2', '/network/users/(?P<id>\d+)', array(
            'methods' => WP_REST_Server::EDITABLE,
            'callback' => array($this, 'update_network_user'),
            'permission_callback' => array($this, 'update_network_user_permissions_check'),
            'args' => array(
                'id' => array(
                    'validate_callback' => 'is_numeric'
                ),
            )
        ));
    }

    /**
     * Endpoint for finding users at the network level (WP REST API works
     * at a sub-site level).
     *
     * @param WP_REST_Request $request
     * @return \WP_Error
     */
    function get_network_users(WP_REST_Request $request)
    {
        if ( isset( $request['email']) ) {
            $user = $this->kore_database->getUserByEmail( $request['email'] );
        } else {
            return new WP_Error( 'rest_network_user_cannot_filter', __( 'Missing field `email` expected.' ), array( 'status' => 400 ) );
        }

        // IF the User is not found THEN return.
        if (empty($user)) { return null; }

        // Retrieve roles from User data.
        $user_data = get_userdata($user->ID);

        // Convert data to match mappings in WP_REST_Users_Controller.
        return [
            'email' => $user->user_email,
            'id' => $user->ID,
            'name' => $user->display_name,
            'registered_date' => date( 'c', strtotime( $user->user_registered ) ),
            'slug' => $user->user_nicename,
            'url' => $user->user_url,
            'username' => $user->user_login,
            'roles' => $user_data->roles,
        ];
    }

    /**
     * Checks if a given request has access to update a user.
     *
     * @param WP_REST_Request $request Full details about the request.
     * @return true|WP_Error True if the request has access to update the item, WP_Error object otherwise.
     */
    function get_network_users_permissions_check(WP_REST_Request $request)
    {
        if ( ! current_user_can('manage_network_users')) {
            return new WP_Error('rest_network_user_cannot_view', __('Sorry, you cannot search users.'), array('status' => 403));
        }

        return true;
    }

    /**
     * Endpoint for editing users at the network level (WP REST API works
     * at a sub-site level).
     *
     * IMPORTANT: the provider is determined by the URL through which the request comes,
     * e.g. example.com/<PROVIDER>/wp-json/wp/v2/network/users/1. In cases where the root
     * URL is used Wordpress will fallback to the default blog site.
     *
     * @param WP_REST_Request $request
     * @return WP_ERROR|WP_REST_REPONSE
     */
    function update_network_user(WP_REST_Request $request)
    {
        // get parameters from the request
        $role = $request->get_param('role');
        $user_id = $request->get_param('id');

        // associating user to provider's blog
        if ( ! empty($role) && ! empty($user_id)) {
            return $this->associate_network_user_with_blog($role, $user_id);
        }

        // default error
        return new WP_Error( 'rest_network_user_cannot_determine_request_operation', __( 'The request lacked sufficient data to perform any kind of request.' ), array( 'status' => 400 ) );
    }

    /**
     * Checks if a given request has access to update a user.
     *
     * @param WP_REST_Request $request Full details about the request.
     * @return true|WP_Error True if the request has access to update the item, WP_Error object otherwise.
     */
    function update_network_user_permissions_check(WP_REST_Request $request)
    {
        if ( ! empty($request['roles'] ) && ! current_user_can( 'manage_network_users' )) {
            return new WP_Error('rest_network_user_cannot_manage_network_users', __('Sorry, you cannot manage network users.'), array('status' => 403));
        }

        return true;
    }

    /**
     * Associates the User to the blog through which this request originated.
     * Uses the Kore settings to determine whether the blog is Kore enabled before proceeding.
     *
     * @param string $role
     * @param int $user_id
     * @return WP_ERROR|WP_REST_REPONSE
     */
    private function associate_network_user_with_blog($role, $user_id)
    {
        $settings = \Kore\Plugin\Helpers::settings();
        if (is_null($settings)) {
            return new WP_Error(
                'rest_network_user_cannot_find_kore_settings',
                __( 'The Kore settings have not yet been defined in Wordpress.' ),
                array( 'status' => 500 )
            );
        }

        // identify the provider/blog that the role is being set for
        $current_blog_id = new \Kore\Plugin\Common\BlogId(get_current_blog_id());

        // IF current blog is not Kore enabled THEN throw a response error.
        if ( ! $settings->siteIsKoreEnabled(new \Kore\Plugin\Common\BlogId($current_blog_id))) {
            return new WP_Error(
                'rest_network_user_cannot_associate_to_non_kore_enabled_blog',
                __( 'Cannot fulfil request to associate user with blog as the provider is not Kore enabled, please check Kore SSO settings in Wordpress.' ),
                array( 'status' => 400 )
            );
        }

        // update user with role
        $result = wp_update_user(array('ID' => $user_id, 'role' => $role));
        if ( ! is_wp_error( $result )) {

            // return the User object as a response, leverage the existing API here for consistency
            $user = get_userdata($user_id);
            $api_users = new WP_REST_Users_Controller;
            $request['context'] = 'edit';
            $response = $api_users->prepare_item_for_response($user, $request);
            $response = rest_ensure_response($response);
            return $response;
        }

        // default failure fallback
        return new WP_Error(
            'rest_network_user_cannot_associate_to_blog',
            __('Could not associate user to the given provider blog.'),
            array('status' => 400)
        );
    }
}
