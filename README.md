# Kore Wordpress

WordPress plugin that replaces the native authentication flow with [UBS Kore](https://bitbucket.org/hbrown/ubs-kore) SSO.

## Versions

| Plugin Version | PHP Support     |
|----------------|-----------------|
| V4             | PHP 8.x         |
| V3             | PHP 5.6 and 7.x |

## PHP Dependency Requirements

The following PHP dependencies are required:
```text
ext-curl
ext-openssl
```

## Installation

* Install the plugin as normal in WordPress
* Go to Kore SSO > Settings and fill out the credentials on this page.
* Check your WordPress site has a valid `.htaccess` file.

## Creating a package for the client to upload

1. Clone the repository
2. Run `composer install`
3. Create a `.zip` of the plugin (including the `vendor` directory)
4. Upload the package content to `wp-content/plugins`

## Kore Auth Server Verification

By default, the Kore plugin will verify the identity of the Kore auth server URL before attempting to send the user there. 
This protects the user against masquerading attacks whereby an attacker sets up a website to look like Kore in order to harvest their login credentials.
If for whatever reason you need to disable this check (e.g. local development), you can add the following to the wp-config.php file.
```php
define( 'KORE_DISABLE_SERVER_VERIFICATION', true );
```
