<?php

/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 */
class Kore_Activator {

    private static $kore_database;
    
    public static function activate() {
    
        // init instance of kore database
        Kore_Activator::$kore_database = new Kore_Database();
        
        // run activation commands
        // ...
    }
}