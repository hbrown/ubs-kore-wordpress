<?php

use \Kore\Plugin\Settings\SiteSettingsStore;
use \Kore\Plugin\Settings\MultisiteSettingsStore;

/**
 * Fired during plugin de-activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 */
class Kore_Deactivator {

    private static $kore_database;

    public static function deactivate()
    {
        (new SiteSettingsStore)->delete();
        (new MultisiteSettingsStore)->delete();
    }
}
