<?php

/**
 * Certain aspects of the Kore plugin require database access for the storing
 * of credentials, and authentication. Some of these calls need to be available
 * to both the public and admin side of the plugin. This class works as a
 * central repository for working with the wordpress database.
 *
 */
class Kore_Database {

    private $wpdb;

    public function __construct() {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    /**
     * Retrieves a user by email at the network level (so across all sites).
     *
     * @param string $email
     * @return object|null
     */
    public function getUserByEmail($email)
    {
        $user = $this->wpdb->get_row(
            $this->wpdb->prepare(
                "SELECT *
                 FROM {$this->wpdb->users}
                 WHERE user_email = %s
                 LIMIT 0, 1",
                $email
        ));

        if ( ! empty($user)) {
            // probably not a good idea to return these, so unset them
            unset(
                $user->user_pass,
                $user->user_activation_key
            );
        }

        return $user;
    }
}
