<?php
namespace Kore\Infrastructure;

use Assert\Assertion;

class Configuration
{
    private $base_url;
    private $property_id;

    /**
     * @param string $base_url
     * @param string $property_id
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(
        $base_url,
        $property_id
    ) {
        Assertion::url($base_url);
        Assertion::notBlank($property_id);

        $this->base_url = $base_url;
        $this->property_id = $property_id;
    }

    /** @return string */
    public function getBaseUrl()
    {
        return $this->base_url;
    }

    /** @return string */
    public function getPropertyId()
    {
        return $this->property_id;
    }
}
