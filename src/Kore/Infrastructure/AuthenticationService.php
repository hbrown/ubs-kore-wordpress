<?php
namespace Kore\Infrastructure;

use Carbon\CarbonImmutable;
use GuzzleHttp\Client;
use Lcobucci\JWT\JwtFacade;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token\RegisteredClaims;
use Lcobucci\JWT\UnencryptedToken;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\LooseValidAt;
use Lcobucci\JWT\Validation\Constraint\PermittedFor;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\Clock\FrozenClock;

class AuthenticationService
{
    public function __construct(
        private Configuration $config,
        private AuthTokenLog $authTokens,
    ) {}

    /** @return string */
    public function getHomeUrl()
    {
        return $this->config->getBaseUrl();
    }

    /**
     * @param string $return_url
     * @return string
     */
    public function getLoginUrl($return_url = null)
    {
        $query_params = '';
        if ( ! empty($return_url)) {
            $query_params = sprintf('?returnUrl=%s', $return_url);
        }

        return sprintf(
            '%s/properties/%s/login%s',
            $this->config->getBaseUrl(),
            $this->config->getPropertyId(),
            $query_params
        );
    }

    public function handleResponseToken(string $token) : string
    {
        $key = $this->getPublicSigningKey();

        $token = (new JwtFacade)->parse(
            $token,
            new SignedWith(new Sha256, $key),
            new LooseValidAt(new FrozenClock(CarbonImmutable::now()->toDateTimeImmutable())),
            new IssuedBy($this->config->getBaseUrl()),
            new PermittedFor(rtrim(get_site_url(), '/')),
        );

        $this->preventReplayAttacks($token);

        return $this->resolveUsername($token);
    }

    private function preventReplayAttacks(UnencryptedToken $token) : void
    {
        if ($this->authTokens->exists($token)) {
            throw new AuthTokenFailedReplayAttackValidation($token);
        }

        $this->authTokens->record($token);
    }

    private function getPublicSigningKey() : Key
    {
        $url = sprintf(
            '%s/api/public-keys/wordpress/%s',
            $this->config->getBaseUrl(),
            $this->config->getPropertyId()
        );

        $client = new Client;
        $request = $client->get($url);
        $response = $request->getBody();

        $contents = json_decode($response->getContents());
        $key = $contents->data->attributes->public_key;

        return InMemory::plainText($key);
    }

    private function resolveUsername(UnencryptedToken $token)
    {
        $user = $this->findUserByKoreUserId(
            $token->claims()->get(RegisteredClaims::SUBJECT)
        );

        if (is_null($user)) {
            $user = $this->findUserByEmail(
                $token->claims()->get('email')
            );
        }

        // IMPORTANT NOTE: currently leverage this functionality to update
        // users via an API like request from Kore. So that their info can
        // be updated without them needing to login.
        // See KorePublic::kore_user_update.

        // IF user was found
        // THEN sync their profile with the token info.
        if ($user) {
            $this->updateUser($user, $token);
        }

        // IF user was not found
        // THEN register their profile using the token info.
        if (is_null($user)) {
            $user = $this->registerUser($token);
        }

        if ( ! $this->hasUserBeenRegisteredWithCurrentBlog($user)) {
            $this->registerUserWithCurrentBlog($user);
        }

        return $user->user_login;
    }

    private function findUserByKoreUserId($koreUserId)
    {
        $users = get_users(array(
            'meta_key' => 'kore_user_id',
            'meta_value' => $koreUserId,
        ));

        if (empty($users)) { return null; }

        return array_shift($users);
    }

    private function findUserByEmail($email)
    {
        return (new \Kore_Database)->getUserByEmail($email);
    }

    private function registerUser(UnencryptedToken $token)
    {
        $data = [
            'user_login' => strtolower(sprintf(
                '%s%s_%s',
                substr($token->claims()->get('given_name'), 0, 1),
                $token->claims()->get('family_name'),
                time()
            )),
            'user_pass' => null,
            'user_email' => $token->claims()->get('email'),
            'display_name' => $token->claims()->get('name'),
            'first_name' => $token->claims()->get('given_name'),
            'last_name' => $token->claims()->get('family_name'),
        ];

        $id = wp_insert_user($data);

        add_user_meta($id, 'kore_user_id', $token->claims()->get(RegisteredClaims::SUBJECT));

        return get_user_by('id', $id);
    }

    private function updateUser($user, UnencryptedToken $token) : void
    {
        $data = [
            'ID' => $user->ID,
            'user_email' => $token->claims()->get('email'),
            'display_name' => $token->claims()->get('name'),
            'first_name' => $token->claims()->get('given_name'),
            'last_name' => $token->claims()->get('family_name'),
        ];

        wp_update_user($data);

        update_user_meta($user->ID, 'kore_user_id', $token->claims()->get(RegisteredClaims::SUBJECT));
    }

    /**
     * When dealing with multisite installations the user will be initially
     * registered against whichever sub-site they are attempting to access. If
     * they then attempt to access a second sub-site (from Kore) they won't yet
     * have been associated to that site, so this method and the
     * `registerUserWithCurrentBlog` method handle that assignment.
     *
     * @improvement abstract this method.
     *
     * @param $user
     * @return bool
     */
    private function hasUserBeenRegisteredWithCurrentBlog($user)
    {
        $current_blog_id = get_current_blog_id();
        return is_user_member_of_blog($user->ID, $current_blog_id);
    }

    /**
     * @improvement abstract this method.
     * @see self::hasUserBeenRegisteredWithCurrentBlog
     *
     * @param $user
     * @return void
     */
    private function registerUserWithCurrentBlog($user)
    {
        wp_update_user([
            'ID' => $user->ID,
            'role' => 'subscriber',
        ]);
    }
}
