<?php
namespace Kore\Infrastructure;

class ServerExistenceChecker
{
    /**
     * @param string $base_url
     * @return bool
     */
    public static function baseUrlExistsAndIsRunningKore($base_url)
    {
        $url = sprintf('%s/health/running', $base_url);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $curl_error = curl_errno($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($curl_error || $http_code !== 200) { return false; }

        return true;
    }
}
