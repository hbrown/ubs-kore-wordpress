<?php
namespace Kore\Infrastructure;

use Lcobucci\JWT\Token\RegisteredClaims;
use Lcobucci\JWT\UnencryptedToken;

class AuthTokenLog
{
    const TABLE = 'kore_auth_token_log';

    public function exists(UnencryptedToken $token) : bool
    {
        $nonce = $token->claims()->get(RegisteredClaims::ID);

        $used_tokens = $this->getTokens();

        $matches = array_filter($used_tokens, function($used_token) use ($nonce) {
            return $used_token->nonce === $nonce;
        });

        return count($matches) > 0;
    }

    public function record(UnencryptedToken $token) : void
    {
        $current_tokens = $this->getTokens();

        // Purge tokens we no longer need to care about
        // (they'll be covered by their expires claim).
        $one_day_ago = time() - 86400;
        $tokens = array_filter($current_tokens, function($token) use ($one_day_ago) {
            return $token->recorded_at > $one_day_ago;
        });

        $tokens[] = (object) [
            'nonce' => $token->claims()->get(RegisteredClaims::ID),
            'recorded_at' => time(),
        ];

        $this->setTokens($tokens);
    }

    private function getTokens() : array
    {
        $option = get_site_option(self::TABLE, null);

        if ( ! is_null($option)) {

            $tokens =  json_decode($option);

            // We've encountered circumstances where this plugin has recorded
            // an associative array of tokens which JSON then considers to be
            // an object. To address this we will convert it back to an array
            // and pull out the array values. This should result in the JSON
            // being persisted correctly moving forward.
            if (is_object($tokens)) {
                $tokens = (array) $tokens;
                $tokens = array_values($tokens);
            }

            return $tokens;
        }

        return [];
    }

    /**
     * Note that the WordPress column for site options has the LONGTEXT type.
     * A quick calculation suggests that we should be able to store up to 57
     * million tokens in such a field. However, given this functionality is
     * self-purging it should never get close to that.
     */
    private function setTokens(array $tokens) : void
    {
        update_site_option(self::TABLE, json_encode($tokens));
    }
}
