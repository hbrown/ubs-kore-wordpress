<?php
namespace Kore\Infrastructure;

use Exception;
use Lcobucci\JWT\UnencryptedToken;

class AuthTokenFailedValidation extends Exception
{
    public function __construct(UnencryptedToken $token)
    {
        parent::__construct(sprintf(
            'Token: %s',
            $token->toString()
        ));
    }
}