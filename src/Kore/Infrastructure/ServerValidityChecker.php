<?php
namespace Kore\Infrastructure;

use Exception;

/**
 * @improvement: if the Wordpress website is ever updated then this class
 * could make use of `spatie/ssl-certificate` akin to Solomon.
 */
class ServerValidityChecker
{
    /**
     * Server Masquerading - A malicious Server might masquerade as the
     * legitimate server using various means. To detect such an attack,
     * the Client needs to authenticate the server.
     *
     * @see https://tools.ietf.org/html/rfc2818#section-3.1
     *
     * @param string $url   The target URL
     * @return bool
     */
    public function isValid($url)
    {
        try {
            $target_domain = parse_url($url, PHP_URL_HOST);
            $get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
            $read = stream_socket_client("ssl://".$target_domain.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
            $cert = stream_context_get_params($read);
            $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
        } catch (Exception $e) {
            return false;
        }

        // [RFC2818]: If a subjectAltName extension of type dNSName is present,
        // that MUST be used as the identity.
        if (isset($certinfo['extensions']['subjectAltName'])) {
            $additional_domains = explode(', ', $certinfo['extensions']['subjectAltName']);
            foreach ($additional_domains as $additional_domain) {
                if ($this->equal($target_domain, $additional_domain)) {
                    return true;
                }
            }
            return false;
        }

        // [RFC2818]: Otherwise, the (most specific) Common Name field in the
        // Subject field of the certificate MUST be used.
        if ( ! empty($certinfo['subject']['CN'])) {
            if (is_string($certinfo['subject']['CN'])) {
                return $this->equal($target_domain, $certinfo['subject']['CN']);
            } elseif (is_array($certinfo['subject']['CN'])) {
                return $this->equal($target_domain, $certinfo['subject']['CN'][0]);
            }
        }

        return false;
    }

    /**
     * @param string $target_domain
     * @param string $remote_domain
     * @return bool
     */
    private function equal(
        $target_domain,
        $remote_domain
    ) {

        // Sometimes the remote domain includes a "DNS:" prefix so we'll strip that.
        $remote_domain = str_replace('DNS:', '', $remote_domain);

        if ($target_domain === $remote_domain) { return true; }

        // When handling wild card domains make sure to only remove the
        // first part of the domain. E.g. *.example.com should match
        // www.example.com but not foo.www.example.com.

        if ($this->startsWith($remote_domain, '*.')) {
            $remote_domain = str_replace('*.', '', $remote_domain);
            $shortened_local_domain = substr($target_domain, strpos($target_domain, '.') + 1);
            if ($shortened_local_domain === $remote_domain) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    private function startsWith ($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
}
