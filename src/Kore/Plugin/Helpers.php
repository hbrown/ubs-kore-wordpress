<?php
namespace Kore\Plugin;

use Assert\Assertion;
use Assert\InvalidArgumentException;
use Kore\Plugin\Settings\MultisiteSettingsStore;
use Kore\Plugin\Settings\SiteSettingsStore;

class Helpers
{
    /** @return bool */
    public static function isMultisite()
    {
        return
            function_exists('is_multisite')
            && is_multisite();
    }

    public static function settings()
    {
        return self::isMultisite()
            ? (new MultisiteSettingsStore)->find()
            : (new SiteSettingsStore)->find();
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isUuid($value)
    {
        if (empty($value)) { return false; }

        if ( ! is_string($value)) { return false; }

        try {
            Assertion::uuid($value);
            return true;
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }
}
