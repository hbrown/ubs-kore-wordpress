<?php
namespace Kore\Plugin\Common;

use Assert\Assertion;

class PropertyId
{
    private $id;

    public function __construct($id)
    {
        Assertion::string($id);
        Assertion::notBlank($id);
        $this->id = $id;
    }

    /** @return bool */
    public function equals(self $candidate)
    {
        return $this->toString() === $candidate->toString();
    }

    /** @return string */
    public function toString()
    {
        return $this->id;
    }

    /** @return string */
    public function __toString()
    {
        return $this->toString();
    }
}
