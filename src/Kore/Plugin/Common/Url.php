<?php
namespace Kore\Plugin\Common;

use Assert\Assertion;

class Url
{
    private $url;

    public function __construct($url)
    {
        Assertion::string($url);
        Assertion::url($url);
        $this->url = $url;
    }

    /** @return bool */
    public function equals(self $candidate)
    {
        return $this->toString() === $candidate->toString();
    }

    /** @return string */
    public function toString()
    {
        return $this->url;
    }

    /** @return string */
    public function __toString()
    {
        return $this->toString();
    }
}
