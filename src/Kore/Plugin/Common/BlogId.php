<?php
namespace Kore\Plugin\Common;

use Assert\Assertion;

class BlogId
{
    private $id;

    public function __construct($id)
    {
        Assertion::numeric($id);
        $this->id = (int) $id;
    }

    /** @return bool */
    public function equals(self $candidate)
    {
        return $this->toString() === $candidate->toString();
    }

    /** @return int */
    public function toInteger()
    {
        return $this->id;
    }

    /** @return string */
    public function toString()
    {
        return (string) $this->id;
    }

    /** @return string */
    public function __toString()
    {
        return $this->toString();
    }
}
