<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\PropertyId;
use Kore\Plugin\Common\Url;

class SiteSettingsFactory
{
    /**
     * @param array $request
     * @return SiteSettings
     */
    public static function fromFormRequestData(array $request)
    {
        return new SiteSettings(
            new AuthServer(
                new Url($request['auth_server_url']),
                $request['auth_server_verify']
            ),
            new PropertyId($request['property']),
            $request['enabled']
        );
    }
}
