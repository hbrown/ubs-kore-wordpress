<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Helpers;

class MultisiteSettingsFormHandler
{
    public function handle(array $request)
    {
        $errors = [];

        $data = [
            'auth_server_url' => ! empty($request['auth_server_url']) ? $request['auth_server_url'] : null,
            'auth_server_verify' => ! empty($request['auth_server_verify']),
            'sites' => ! empty($request['sites']) ? $request['sites'] : null,
        ];

        if (empty($data['auth_server_url'])) {
            $errors['auth_server_url'] = 'Please enter a value for this field.';
        }

        foreach ($data['sites'] as &$site) {

            // Validate Property IDs.
            if ( ! empty($site['property']) && ! Helpers::isUuid($site['property'])) {
                $errors['sites'][$site['blog']]['property'] = 'Please enter a valid Property ID (UUID) for this field.';
            }

            // Convert "enabled" checkboxes to boolean values.
            if (isset($site['enabled'])) {
                $site['enabled'] = ! empty($site['enabled']);
                continue;
            }
            $site['enabled'] = false;
        }
        unset($site);

        return (object) [
            'data' => $data,
            'errors' => $errors,
        ];
    }
}
