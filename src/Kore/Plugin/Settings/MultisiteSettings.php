<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\BlogId;

/** Holds the configuration options for a Wordpress multi-site install. */
class MultisiteSettings
{
    private $authServer;
    private $sites;

    public function __construct(
        AuthServer $authServer,
        array $sites
    ) {
        $this->authServer = $authServer;
        $this->sites = $sites;
    }

    /** @return AuthServer */
    public function authServer()
    {
        return $this->authServer;
    }

    /** @return array|MultisiteSiteSettings[] */
    public function sites()
    {
        return $this->sites;
    }

    /**
     * @param BlogId $candidate
     * @return bool
     */
    public function siteIsKoreEnabled(BlogId $candidate)
    {
        $site = $this->specificToSite($candidate);
        return $site->enabled();
    }

    /**
     * @param BlogId $candidate
     * @return MultisiteSiteSettings|null
     */
    public function specificToSite(BlogId $candidate)
    {
        foreach ($this->sites() as $site) {
            if ($site->blog()->equals($candidate)) {
                return $site;
            }
        }
        return null;
    }
}
