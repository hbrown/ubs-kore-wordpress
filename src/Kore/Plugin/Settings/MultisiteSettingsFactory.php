<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\BlogId;
use Kore\Plugin\Common\PropertyId;
use Kore\Plugin\Common\Url;

class MultisiteSettingsFactory
{
    /**
     * @param array $request
     * @return MultisiteSettings
     */
    public static function fromFormRequestData(array $request)
    {
        return new MultisiteSettings(
            new AuthServer(
                new Url($request['auth_server_url']),
                $request['auth_server_verify']
            ),
            array_map(function($site) {
                return new MultisiteSiteSettings(
                    new BlogId($site['blog']),
                    ! empty($site['property']) ? new PropertyId($site['property']) : null,
                    $site['enabled']
                );
            }, $request['sites'])
        );
    }
}
