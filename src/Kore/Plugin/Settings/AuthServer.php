<?php
namespace Kore\Plugin\Settings;

use Assert\Assertion;
use Kore\Plugin\Common\Url;

class AuthServer
{
    private $url;
    private $verify;

    public function __construct(
        Url $url,
        $verify
    ) {
        Assertion::boolean($verify);

        $this->url = $url;
        $this->verify = $verify;
    }

    /** @return Url */
    public function url()
    {
        return $this->url;
    }

    /**
     * @deprecated use AuthServerVerification
     * @return bool
     */
    public function shouldBeVerified()
    {
        return $this->verify;
    }
}
