<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\BlogId;
use Kore\Plugin\Common\PropertyId;
use Kore\Plugin\Common\Url;

class MultisiteSettingsStore
{
    public function find()
    {
        $url = get_site_option('kore_auth_server_url');
        $verify = get_site_option('kore_auth_server_verify');
        $sites = get_site_option('kore_sites');

        if ( ! isset($url) || empty($url)) { return null; }

        $sites = json_decode($sites, JSON_OBJECT_AS_ARRAY);
        $sites = array_map(function(array $site) {
            $site = (object) $site;
            return new MultisiteSiteSettings(
                new BlogId($site->blog_id),
                ! empty($site->property_id) ? new PropertyId($site->property_id) : null,
                (bool) $site->enabled
            );
        }, $sites);

        return new MultisiteSettings(
            new AuthServer(
                new Url($url),
                (bool) $verify
            ),
            $sites
        );
    }

    public function exists()
    {
        return ! is_null($this->find());
    }

    public function save(MultisiteSettings $settings)
    {
        $sites = array_map(function(MultisiteSiteSettings $site) {
            return (object) [
                'blog_id' => $site->blog()->toInteger(),
                'property_id' => $site->hasProperty() ? $site->property()->toString() : null,
                'enabled' => $site->enabled(),
            ];
        }, $settings->sites());
        $sites = json_encode($sites);

        if ($this->exists()) {
            update_site_option('kore_auth_server_url', $settings->authServer()->url()->toString());
            update_site_option('kore_auth_server_verify', $settings->authServer()->shouldBeVerified());
            update_site_option('kore_sites', $sites);
            return;
        }

        add_site_option('kore_auth_server_url', $settings->authServer()->url()->toString());
        add_site_option('kore_auth_server_verify', $settings->authServer()->shouldBeVerified());
        add_site_option('kore_sites', $sites);
    }

    public function delete()
    {
        delete_site_option('kore_auth_server_url');
        delete_site_option('kore_auth_server_verify');
        delete_site_option('kore_sites');
    }
}
