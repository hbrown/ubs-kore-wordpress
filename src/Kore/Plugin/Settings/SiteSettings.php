<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\PropertyId;

/** Holds the configuration options for a standard Wordpress install. */
class SiteSettings
{
    private $authServer;
    private $property;
    private $enabled;

    public function __construct(
        AuthServer $authServer,
        PropertyId $property,
        $enabled
    ) {
        $this->authServer = $authServer;
        $this->property = $property;
        $this->enabled = $enabled;
    }

    /** @return AuthServer */
    public function authServer()
    {
        return $this->authServer;
    }

    /** @return PropertyId */
    public function property()
    {
        return $this->property;
    }

    /** @return bool */
    public function enabled()
    {
        return $this->enabled;
    }
}
