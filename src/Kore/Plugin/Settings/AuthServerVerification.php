<?php
namespace Kore\Plugin\Settings;

class AuthServerVerification
{
    /** @return bool */
    public static function isEnabled()
    {
        return ! self::isDisabled();
    }

    /**
     * We don't expect anyone to disable this other than local developers,
     * which is why it's configured via a global variable rather than through
     * the plugin settings page, i.e. admins should have to ask us whether its
     * possible to disable.
     *
     * @return bool
     */
    public static function isDisabled()
    {
        if ( ! defined('KORE_DISABLE_SERVER_VERIFICATION')) {
            return false;
        }

        return KORE_DISABLE_SERVER_VERIFICATION === true;
    }
}
