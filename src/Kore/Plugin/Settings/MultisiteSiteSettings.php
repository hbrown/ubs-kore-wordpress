<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\BlogId;
use Kore\Plugin\Common\PropertyId;

/**
 * Holds the configuration options for a sub-site
 * of a Wordpress multi-site install.
 */
class MultisiteSiteSettings
{
    private $blog;
    private $property;
    private $enabled;

    public function __construct(
        BlogId $blog,
        PropertyId $property = null,
        $enabled = null
    ) {
        $this->blog = $blog;
        $this->property = $property;
        $this->enabled = isset($enabled) ? $enabled : false;
    }

    /** @return BlogId */
    public function blog()
    {
        return $this->blog;
    }

    /** @return bool */
    public function hasProperty()
    {
        return ! is_null($this->property());
    }

    /** @return PropertyId */
    public function property()
    {
        return $this->property;
    }

    /** @return bool */
    public function enabled()
    {
        return $this->enabled;
    }
}
