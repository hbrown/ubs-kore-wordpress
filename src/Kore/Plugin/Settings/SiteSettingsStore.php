<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Common\PropertyId;
use Kore\Plugin\Common\Url;

class SiteSettingsStore
{
    public function find()
    {
        $url = get_site_option('kore_auth_server_url');
        $verify = get_site_option('kore_auth_server_verify');
        $property = get_site_option('kore_property_id');
        $enabled = get_site_option('kore_enabled');

        if ( ! isset($url) || empty($url)) { return null; }

        return new SiteSettings(
            new AuthServer(
                new Url($url),
                (bool) $verify
            ),
            new PropertyId($property),
            (bool) $enabled
        );
    }

    public function exists()
    {
        return ! is_null($this->find());
    }

    public function save(SiteSettings $settings)
    {
        if ($this->exists()) {
            update_site_option('kore_auth_server_url', $settings->authServer()->url()->toString());
            update_site_option('kore_auth_server_verify', $settings->authServer()->shouldBeVerified());
            update_site_option('kore_property_id', $settings->property()->toString());
            update_site_option('kore_enabled', $settings->enabled());
            return;
        }

        add_site_option('kore_auth_server_url', $settings->authServer()->url()->toString());
        add_site_option('kore_auth_server_verify', $settings->authServer()->shouldBeVerified());
        add_site_option('kore_property_id', $settings->property()->toString());
        add_site_option('kore_enabled', $settings->enabled());
    }

    public function delete()
    {
        delete_site_option('kore_auth_server_url');
        delete_site_option('kore_auth_server_verify');
        delete_site_option('kore_property_id');
        delete_site_option('kore_enabled');
    }
}
