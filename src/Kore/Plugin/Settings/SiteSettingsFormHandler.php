<?php
namespace Kore\Plugin\Settings;

use Kore\Plugin\Helpers;

class SiteSettingsFormHandler
{
    public function handle(array $request)
    {
        $errors = [];

        $data = [
            'auth_server_url' => ! empty($request['auth_server_url']) ? $request['auth_server_url'] : null,
            'auth_server_verify' => ! empty($request['auth_server_verify']),
            'property' => ! empty($request['property']) ? $request['property'] : null,
            'enabled' => ! empty($request['enabled']),
        ];

        if (empty($data['auth_server_url'])) {
            $errors['auth_server_url'] = 'Please enter a value for this field.';
        }

        if (empty($data['property'])) {
            $errors['property'] = 'Please enter a value for this field.';
        }

        if ( ! empty($data['property']) && ! Helpers::isUuid($data['property'])) {
            $errors['property'] = 'Please enter a valid Property ID (UUID) for this field.';
        }

        return (object) [
            'data' => $data,
            'errors' => $errors,
        ];
    }
}
