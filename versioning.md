# Versioning

The UBS Kore WordPress plugin loosely conforms to [semantic versioning](https://semver.org/) principles.  
We say loosely because we don't always utilise __PATCH__ numbers.

### 4.1

Contains a minor adjustment to handle circumstances where the AuthTokenLog has persisted used tokens as an associative array. JSON then reads this as an object instead of the desired array, so we simply convert it back to an array, and ignore the key indexes.

### 4.0

Updates composer dependencies allowing for the support of PHP 8.1 and above.  

Developed against PHP 8.3.

Now requires RSA JWT verifications keys with a length of at least 2048 bits.