<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @wordpress-plugin
 * Plugin Name:       UBS Kore
 * Plugin URI:        http://surupartners.com/work/ubs-kore
 * Description:       Overrides the standard WordPress login functionality to use UBS Kore instead. Also supports multisite WP installs as a <a href="/wp-admin/network/plugins.php">network plugin</a>.
 * Version:           4.1
 * Author:            Harrison Brown, Suru Partners Ltd.
 * Author URI:        http://surupartners.com
 * Network: true
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The class which handles database requests for the plugin.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kore-database.php';

/**
 * The code that runs during plugin activation.
 */
function activate_kore() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-kore-activator.php';
    Kore_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_kore() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-kore-deactivator.php';
    Kore_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kore' );
register_deactivation_hook( __FILE__, 'deactivate_kore' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kore.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_kore() {
    $kore = new Kore();
    $kore->run();
}

run_kore();
