<?php
/**
 * The template for displaying login errors with Kore SSO.
 *
 * It is based off the Twenty_Fifteen theme. It can be overriden by copying to
 * the local theme folder.
 * 
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="page-content">

				<h1>Login Error</h1>
				<p><?php echo $wp_query->query_vars['error_message']; ?></p>

			</div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>