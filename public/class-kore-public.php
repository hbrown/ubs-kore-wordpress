<?php

use Kore\Infrastructure\AuthenticationService;
use Kore\Infrastructure\AuthTokenLog;
use Kore\Infrastructure\Configuration;
use Kore\Infrastructure\ServerValidityChecker;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 */
class Kore_Public {

    /**
     * The ID of this plugin.
     *
     * @var  string  $plugin_name  The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @var  string  $version  The current version of this plugin.
     */
    private $version;

    private $kore_database;

    /**
     * Initialize the class and set its properties.
     *
     * @param  string  $plugin_name  The name of the plugin.
     * @param  string  $version      The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->kore_database = new Kore_Database();
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     */
    public function enqueue_styles() {
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kore-public.css', array(), $this->version, 'all' );
    }

    public function url_hijacking()
    {
        if (isset($_SERVER['REQUEST_URI'])) {

            // get url path
            $current_url = $_SERVER['REQUEST_URI'];

            // login return (from Kore)
            if (strpos($current_url, '/kore/login') !== false) {
                $this->kore_return_handler();
                return;
            }

            // login request
            if (strpos($current_url, '/login') !== false) {
                $this->login();
                return;
            }

            if (strpos($current_url, '/kore/users/upsert') !== false) {
                $this->kore_user_upsert();
                return;
            }
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    private function get_kore_sso_config()
    {
        $settings = \Kore\Plugin\Helpers::settings();
        if (empty($settings)) {
            throw new Exception('Kore settings not defined.');
        }

        if (\Kore\Plugin\Helpers::isMultisite()) {

            global $blog_id; // The blog being accessed.

            $siteSettings = $settings->specificToSite(new \Kore\Plugin\Common\BlogId($blog_id));

            if (is_null($siteSettings)) {
                throw new Exception('Kore integration has not been configured for network sub-site.');
            }
            if ( ! $siteSettings->enabled()) {
                throw new Exception('Kore integration has not been enabled for network sub-site.');
            }
            if ( ! $siteSettings->hasProperty()) {
                throw new Exception('Kore integration is not fully configured for network sub-site.');
            }

            return array(
                'base_url' => $settings->authServer()->url()->toString(),
                'validate_auth_server' => $settings->authServer()->shouldBeVerified(),
                'property' => $siteSettings->property()->toString(),
            );
        }

        if ( ! $settings->enabled()) {
            throw new Exception('Kore integration is disabled.');
        }

        if (is_null($settings->property())) {
            throw new Exception('Kore integration is not fully configured.');
        }

        return array(
            'base_url' => $settings->authServer()->url()->toString(),
            'validate_auth_server' => $settings->authServer()->shouldBeVerified(),
            'property' => $settings->property()->toString(),
        );
    }

    /**
     * Because we don't have the IoC container in Wordpress.
     *
     * @param array $raw_config
     * @return AuthenticationService
     * @throws \Assert\AssertionFailedException
     */
    private function instantiateAuthenticationService(array $raw_config)
    {
        $config = new Configuration(
            $raw_config['base_url'],
            $raw_config['property']
        );

        return new AuthenticationService(
            $config,
            new AuthTokenLog,
        );
    }

    /**
     * Handles the initial login request from a user, passing them onto Kore.
     */
    private function login()
    {
        try {

            $config = $this->get_kore_sso_config();
            $auth_service = $this->instantiateAuthenticationService($config);
            $login_url = $auth_service->getLoginUrl();

            if (\Kore\Plugin\Settings\AuthServerVerification::isEnabled()) {
                if ( ! (new ServerValidityChecker)->isValid($login_url)) {
                    throw new \Exception(sprintf(
                        'Target host %s failed validation.',
                        parse_url($login_url, PHP_URL_HOST)
                    ));
                }
            }

            // Append return URL if one is set
            if ( ! empty($_GET['redirect_to'])) {
                $login_url = add_query_arg('returnUrl', $_GET['redirect_to'], $login_url);
            }
        }
        catch (Exception $e) {
            error_log(sprintf('Exception: %s' . PHP_EOL . 'Message: %s' . PHP_EOL . 'File: %s:%s' . PHP_EOL . 'Stack trace:' . PHP_EOL . '%s', get_class($e), $e->getMessage(), __FILE__, __LINE__, $e->getTraceAsString()));
            $this->show_error_page('Something went wrong.', 500);
        }

        // redirect to Kore login
        wp_redirect($login_url);
        exit;
    }

    private function kore_return_handler()
    {
        // if blog doesn't implement Kore then block request
        if ( ! $this->blogImplementsKore()) {
            $this->show_error_page('Kore SSO is disabled for this site.', 403);
        }

        try {

            // check token was supplied
            if (empty($_GET['token'])) {
                throw new InvalidArgumentException('Authentication token was not supplied.');
            }

            $config = $this->get_kore_sso_config();
            $auth_service = $this->instantiateAuthenticationService($config);
            $username = $auth_service->handleResponseToken($_GET['token']);
        }
        catch (InvalidArgumentException $e) {
            $this->show_error_page($e->getMessage(), 400);
            exit;
        }
        catch (Exception $e) {
            error_log(sprintf('Exception: %s' . PHP_EOL . 'Message: %s' . PHP_EOL . 'File: %s:%s' . PHP_EOL . 'Stack trace:' . PHP_EOL . '%s', get_class($e), $e->getMessage(), __FILE__, __LINE__, $e->getTraceAsString()));
            $this->show_error_page('Could not handle auth request from UBS Kore.', 500);
            exit;
        }

        try {

            // find the user
            $user = get_user_by( 'login', $username );
            if ( !empty( $user ) && !is_wp_error( $user ) ) {

                // authenticate the user
                wp_clear_auth_cookie();
                wp_set_current_user( $user->ID );
                wp_set_auth_cookie( $user->ID, true );

                // trigger login action
                do_action( 'wp_signon' );

                // redirect to return url or home page
                $redirect_to = ( ! empty($_GET['returnUrl'])) ? $_GET['returnUrl'] : home_url();

                wp_redirect($redirect_to);
                exit;
            }

            // if they haven't redirect yet, then something went wrong
            $this->show_error_page('User authentication failed.', 401);

        }
        catch (Exception $e) {
            error_log(sprintf('Exception: %s' . PHP_EOL . 'Message: %s' . PHP_EOL . 'File: %s:%s' . PHP_EOL . 'Stack trace:' . PHP_EOL . '%s', get_class($e), $e->getMessage(), __FILE__, __LINE__, $e->getTraceAsString()));
            $this->show_error_page('Could not authenticate user.', 500);
        }
    }

    /**
     * This method is intended for updating Users when they change in Kore.
     * It works in the same manner as auto-provisioning via login, whereby the
     * incoming request will have a JWT token which can be verified as having
     * come from UBS Kore. WP can then use the content of that JWT to create
     * or update the User.
     */
    private function kore_user_upsert()
    {
        if ( ! $this->blogImplementsKore()) {
            wp_send_json(['error' => 'Kore SSO is not enabled'], 400);
        }

        try {

            if (empty($_GET['token'])) {
                wp_send_json(['error' => 'Authentication token was not supplied'], 400);
            }

            $config = $this->get_kore_sso_config();
            $auth_service = $this->instantiateAuthenticationService($config);

            // leverage the token handler here which will internally update
            // the user's credentials (i.e. name, email, etc).
            $auth_service->handleResponseToken($_GET['token']);

            wp_send_json(['success' => true], 200);
        }
        catch (InvalidArgumentException $e) {
            wp_send_json(['error' => $e->getMessage()], 400);
        }
        catch (Exception $e) {
            wp_send_json(['error' => 'Something went wrong'], 500);
        }
    }

    /**
     * Handles user logout request.
     */
    public function handle_wordpress_logout()
    {
        // if blog implements Kore then use plugin functionality
        if ($this->blogImplementsKore()) {

            $config = $this->get_kore_sso_config();
            $auth_service = $this->instantiateAuthenticationService($config);
            $home_url = $auth_service->getHomeUrl();

            // redirect back to Kore home
            wp_redirect($home_url);
            exit;
        }
    }

    /**
     * Handles user login request.
     */
    public function handle_wordpress_login()
    {
        if ($this->blogImplementsKore()) {
            $this->login();
        }
        return;
    }

    /**
     * Checks to see whether blog is specified as using Kore.
     *
     * @return boolean
     */
    private function blogImplementsKore()
    {
        global $blog_id;

        $settings = \Kore\Plugin\Helpers::settings();
        if (is_null($settings)) { return false; }

        if (\Kore\Plugin\Helpers::isMultisite()) {
            // IF dealing with multisite installation
            // THEN check specific blog being accessed is Kore enabled.
            return $settings->siteIsKoreEnabled(new \Kore\Plugin\Common\BlogId($blog_id));
        } else {
            return $settings->enabled();
        }
    }

    public function show_error_page($error_message = '', $status_code = 500)
    {
        // add HTTP status code
        status_header($status_code);

        // add message to global vars
        global $wp_query;
        $wp_query->query_vars['error_message'] = $error_message;

        // show template
        $template = 'error-login.php';
        if ( $overridden_template = locate_template( $template ) ) {
            // locate_template() returns path to file
            // if either the child theme or the parent theme have overridden the template
            load_template( $overridden_template );
        } else {
            // If neither the child nor parent theme have overridden the template,
            // we load the template from the 'templates' sub-directory of the directory this file is in
            load_template( dirname( __FILE__ ) . '/views/'.$template );
        }
        exit;
    }

    /**
     * Encourage server to work with JSON Basic Authentication plugin (https://github.com/WP-API/Basic-Auth)
     *
     * @return void
     */
    public function basic_auth_configuration( $wp_validate_auth_cookie )
    {
        // REDIRECT_HTTP_AUTHORIZATION is sometimes set instead of HTTP_AUTHORIZATION
        if ( ! empty($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) && empty($_SERVER['HTTP_AUTHORIZATION']))
        {
            $_SERVER['HTTP_AUTHORIZATION'] = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        }

        // If we've got basic authentication in HTTP_AUTHORIZATION but not in the PHP_AUTH_ parameters then set it
        if (
            empty($_SERVER['PHP_AUTH_USER']) &&
            empty($_SERVER['PHP_AUTH_PW']) &&
            isset($_SERVER['HTTP_AUTHORIZATION']) &&
            preg_match('/Basic\s+(.*)$/i', $_SERVER['HTTP_AUTHORIZATION'], $matches)
        )
        {
            list($name, $password) = explode(':', base64_decode($matches[1]));

            $_SERVER['PHP_AUTH_USER'] = strip_tags($name);
            $_SERVER['PHP_AUTH_PW'] = strip_tags($password);
        }

        return $wp_validate_auth_cookie;
    }

}
